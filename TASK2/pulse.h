#ifndef __PULSE_H__
#define __PULSE_H__

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/moduleparam.h>
#include <linux/gpio.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/time.h>

#define US_FLASH_MAGIC 'k'
#define DEVICE_NAME "us_driver"

/*#define FLASHGETS	_IOR(US_FLASH_MAGIC, 1, int)
#define FLASHGETP	_IOR(US_FLASH_MAGIC, 2, int)
#define FLASHSETP	_IOW(US_FLASH_MAGIC, 3, int)
#define FLASHERASE      _IO(US_FLASH_MAGIC, 4)*/

static struct class *us_driver_class = NULL;
static dev_t us_driver_major_number;
static dev_t us_driver_minor_number = 0;

struct us_driver_dev{
	char name[20];
	dev_t devno;
	struct cdev cdev;
	spinlock_t spinlock;
	struct device *dev;
	unsigned int irq_num;
	uint32_t rising_edge_time;
	uint32_t falling_edge_time;
	uint32_t time_difference;
	int can_read;
	int edge;
} *us_driver_devp;

int __init us_driver_init(void);
void __exit us_driver_exit(void);
static int us_driver_open(struct inode *inode, struct file *file);
static int us_driver_release(struct inode *inode, struct file *file);
ssize_t us_driver_write(struct file *file, const char *buf, size_t count, loff_t *ppos);
ssize_t us_driver_read(struct file *file, char *buf, size_t count, loff_t *ppos);
long us_driver_ioctl(struct file *file, unsigned int cmd, unsigned long arg); 
#endif
