/*********************************************************************
 * PULSE ultrasonic driver
 *
 * COPYRIGHT (c) JANAKARAJAN NATARAJAN
 *
 *********************************************************************/
#include "pulse.h"

int init_led(void);
irqreturn_t us_handler(int irq, void *dev_id, struct pt_regs *regs);
uint32_t inline rdtsc(void);

/*******************************************************
 * FUNCTION: Read Time Stamp Counter
 *
 * RETURNS: timestamp value
 *
 *******************************************************/
uint32_t inline rdtsc(){
	unsigned int low, high;
	asm volatile("rdtsc"
			: "=a" (low), "=d" (high));
	return (uint32_t)low;
}

static struct file_operations us_driver_fops = {
	.owner		= THIS_MODULE,
	.open		= us_driver_open,
	.release	= us_driver_release,
	.write		= us_driver_write,
	.read		= us_driver_read,
};

/*****************************************************************
 * FUNCTION: Write trigger pulse to the ultrasonic sensor
 *
 *****************************************************************/
ssize_t us_driver_write(struct file *file, const char *buf,
		size_t count, loff_t *ppos){
	int copied;
	char usr_buf[2];
	copied = copy_from_user(usr_buf, buf, sizeof(usr_buf));
	gpio_set_value(14, 1);									/* Write trigger for 10us */
	udelay(10);										/* ultrasonic sensor uses this to generate ultrasonic pulse */
	gpio_set_value(14, 0);

	if(copied != 0){
		return -ENOMEM;
	}
	return copied;
}

/******************************************************************
 * FUNCTION: Read the global pulse width variable to user buffer
 *
 *****************************************************************/
ssize_t us_driver_read(struct file *file, char *buf,
		size_t count, loff_t *ppos){
	int bytes;
	struct us_driver_dev *us_driver_devp = file->private_data;

	if(us_driver_devp->can_read == 0){
		return -1;
	}
	bytes = copy_to_user(buf, &us_driver_devp->time_difference, 32);			/* Return the pulse width from the global variable */
	us_driver_devp->can_read = 0;
	if(bytes != 0){
		return -ENOMEM;
	}
	return 0;
}

static int us_driver_open(struct inode *inode, struct file *file){
	struct us_driver_dev *us_driver_devp;

	us_driver_devp = container_of(inode->i_cdev, struct us_driver_dev, cdev);
	
	file->private_data = us_driver_devp;
	return 0;
}

static int us_driver_release(struct inode *inode, struct file *file){
	return 0;
}

/*****************************************************************************
 * FUNCTION: Initialize the GPIO for the ultrasonic sensor to work
 *
 * RETURNS: -1  Error
 *           0  Success
 *
 *****************************************************************************/
int init_led(){
	int ret;

	ret = gpio_request_one(31, GPIOF_OUT_INIT_LOW, "us_driver_led_pin1");
	if(ret != 0){
		return -1;
	}
	gpio_set_value_cansleep(31, 0);
	ret = gpio_request_one(30, GPIOF_OUT_INIT_LOW, "us_driver_led_pin2");
	if(ret != 0){
		return -1;
	}
	gpio_set_value_cansleep(30, 0);
	ret = gpio_request_one(14, GPIOF_OUT_INIT_LOW, "us_driver_led_pin3");
	if(ret != 0){
		return -1;
	}
	gpio_set_value_cansleep(14, 0);
	ret = gpio_request_one(15, GPIOF_IN, "us_driver_led_pin4");
	if(ret != 0){
		return -1;
	}
	gpio_set_value_cansleep(15, 0);

	return 0;
}

/***********************************************************************
 * FUNCTION: Interrupt request handler
 *
 * NOTES: Called when system detects falling or rising edge interrupt
 *        Reads the timestamp value during both interrupts
 *        Puts time difference between the edges into global variable
 *
 ***********************************************************************/
irqreturn_t us_handler(int irq, void *dev_id, struct pt_regs *regs){
	uint64_t time;
	int ret;
	struct us_driver_dev *us_driver_devp = NULL;
	us_driver_devp = (struct us_driver_dev*)dev_id;

	time = rdtsc();
	if(us_driver_devp->edge == 1){
		us_driver_devp->rising_edge_time = time;
		ret = irq_set_irq_type(us_driver_devp->irq_num, IRQ_TYPE_EDGE_FALLING);
		if(ret){
			printk(KERN_ERR "HANDLER FALLING SET ERROR");
		}
		us_driver_devp->edge = 0;
	}
	else{
		us_driver_devp->falling_edge_time = time;
		us_driver_devp->time_difference = time - us_driver_devp->rising_edge_time;
		us_driver_devp->can_read = 1;
		ret = irq_set_irq_type(us_driver_devp->irq_num, IRQ_TYPE_EDGE_RISING);
		if(ret){
			printk(KERN_ERR "HANDLER RISING SET ERROR");
		}
		us_driver_devp->edge = 1;
	}
	return IRQ_HANDLED;
}

int __init us_driver_init(void){
	int error, device_num, ret;

	error = init_led();	
	if(error == -1){
		return -1;
	}

	if(alloc_chrdev_region(&us_driver_major_number, 0, 1, DEVICE_NAME) < 0){
		printk(KERN_DEBUG "Can't register device\n");
		return -1;
	}
	
	us_driver_class = class_create(THIS_MODULE, DEVICE_NAME);
	device_num = MKDEV(MAJOR(us_driver_major_number), us_driver_minor_number);
	us_driver_devp = kmalloc(sizeof(struct us_driver_dev), GFP_KERNEL);
	if(!us_driver_devp){
		return -ENOMEM;
	}
	
	cdev_init(&us_driver_devp->cdev, &us_driver_fops);
	us_driver_devp->cdev.owner	= THIS_MODULE;
	us_driver_devp->devno		= device_num;
	error = cdev_add(&us_driver_devp->cdev, device_num, 1);
	if(error){
		return -1;
	}
	us_driver_devp->dev = device_create(us_driver_class, NULL, device_num, NULL, "pulse");

	us_driver_devp->irq_num = gpio_to_irq(15);
	ret = request_irq(us_driver_devp->irq_num, (irq_handler_t)us_handler, IRQF_TRIGGER_RISING, "us_driver", us_driver_devp);	/* Set the interrupt handler for gpio 15 irq */
	if(ret){
		printk(KERN_ERR "Cannot register IRQ");
	}

	us_driver_devp->edge = 1;
	return 0;
}

void __exit us_driver_exit(void){
	free_irq(us_driver_devp->irq_num, us_driver_devp);										/* Free irq */
	gpio_free(30);
	gpio_free(31);
	gpio_free(14);
	gpio_free(15);
	cdev_del(&us_driver_devp->cdev);
	device_destroy(us_driver_class, us_driver_devp->devno);
	unregister_chrdev_region(us_driver_major_number, 1);
	
	kfree(us_driver_devp);
	class_destroy(us_driver_class);
}

module_init(us_driver_init);
module_exit(us_driver_exit);
MODULE_LICENSE("GPL v2");
