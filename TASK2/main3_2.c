/***********************************************************************
 * main3_2.c ---- User program to test the drivers and display pattern 
 *
 * COPYRIGHT (c) JANAKARAJAN NATARAJAN
 *
 **********************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <poll.h>
#include <inttypes.h>
#include <pthread.h>

#define SPI_IOC_MAGIC 'k'
#define SPI_IOC_USER_PATTERN	_IOW(SPI_IOC_MAGIC, 10, char*)

int main(int argc, char **argv){
	int us_fd, led_fd, i = 0, d;
	int status;
	char buf[2];
	int bits = 8;
	int count = 0;
	int lt40 = 0, lt20 = 0, lt30 = 0, gt40 = 0;
	long long int speed = 10000000;
	char tmp_buf;
	long double time, distance, temp_distance;
	uint32_t read_buf;
	struct spi_ioc_transfer tr;
	int flag = 0;
	/* 10 patterns given to spi_led driver on start */
	char pattern[] = {
        0x01, 0xfc, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0x87, 0x08, 0x82,
        0x01, 0xf8, 0x02, 0x82, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0x87, 0x08, 0x82,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x81, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0x87, 0x08, 0x82,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x81, 0x05, 0x80, 0x06, 0xff, 0x07, 0x87, 0x08, 0x82,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x82, 0x06, 0xff, 0x07, 0x87, 0x08, 0x82,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0x8e, 0x08, 0x84,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0x9c, 0x08, 0x88,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0xb8, 0x08, 0x90,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0xf0, 0x08, 0xa0,
        0x01, 0xf8, 0x02, 0x80, 0x03, 0x80, 0x04, 0x80, 0x05, 0x80, 0x06, 0xff, 0x07, 0xe0, 0x08, 0xc0};

	/* Config buffer used to setup the LED display device */
	char config[10] = {0x0c, 0x01, 0x0f, 0x00, 0x0a, 0x0f, 0x0b, 0x07, 0x09, 0x00}; 
	
	/* Patterns given to the spi_led driver based on the distance calculated from the ultrasonic sensor */
	unsigned char user_pattern[23] = {0, 100, 1, 100, 2, 100, 3, 100, 4, 100, 5, 100, 6, 100, 7, 100, 8, 100, 9, 100, 0, 0};
	unsigned char gt_20_pattern[23] = {0, 100, 1, 100, 2, 100, 3, 100, 4, 100, 4, 100, 3, 100, 2, 100, 1, 100, 0, 100, 0, 0};
	unsigned char gt_40_pattern[23] = {5, 100, 6, 100, 7, 100, 8, 100, 9, 100, 9, 100, 8, 100, 7, 100, 6, 100, 5, 100, 0, 0};

	/* End pattern give to the spi_led driver when distance is less than 5.0cms */
	unsigned char end_pattern[2] = {0, 0};

	us_fd = open("/dev/pulse", O_RDWR);
	if(us_fd < 0){
		printf("\nCould not open US");
		return -1;
	}
	
	led_fd = open("/dev/spidev_driver", O_RDWR);
	if(led_fd < 0){
		printf("\nCould not open LED");
		return -1;
	}

	/* Setup LED display device */
	status = ioctl(led_fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	status = ioctl(led_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	for(i = 0; i < 5; i++){
		d = i;
		buf[0] = config[i + d];
		d += 1;
		buf[1] = config[i + d];
		memset((void*)&tr, 0, sizeof(struct spi_ioc_transfer));
		tr.tx_buf = (unsigned long)buf;
		tr.len = 2;
		tr.bits_per_word = 8;
		tr.speed_hz = 10000000;
		tr.delay_usecs = 1;
		status = ioctl(led_fd, SPI_IOC_MESSAGE(1), &tr);
	}
	
	/* Send user pattern to spi_led driver */
	status = ioctl(led_fd, SPI_IOC_USER_PATTERN, pattern);
	status = read(us_fd, &read_buf, 32);
	
	/* Loop until the distance is less than 5.0cms */
	while(1){
		status = write(us_fd, &tmp_buf, 1);
		if(status < 0){
			printf("\nImproper write");
		}
		status = read(us_fd, &read_buf, 32);
		if(status < 0){
		}
		time = (long double)read_buf/(long double)400;
		distance = (340 * time)	/ 2;
		distance = distance / 10000;
		if(distance < 40.0 && distance > 30.0){
			if(lt40 == 0){
				write(led_fd, gt_40_pattern, 19);
			}
			lt40 = 1;
			lt30 = 0;
			lt20 = 0;
			gt40 = 0;
		}
		else if(distance < 30.0 && distance > 20.0){
			if(lt30 == 0){
				write(led_fd, gt_20_pattern, 23);
			}
			lt30 = 1;
			lt20 = 0;
			lt40 = 0;
			gt40 = 0;
		}
		else if((distance < 20.0 && distance > 10.0) || distance > 40.0){
			if(lt20 == 0 || gt40 == 0){
				write(led_fd, user_pattern, 23);
			}
			gt40 = 1;
			lt20 = 1;
			lt30 = 0;
			lt40 = 0;
		}
		else if(distance < 5.0){
			count++;
			if(count > 5){
				break;
			}
		}
		usleep(10000);
	};
	
	/* Write end pattern to spi_led driver */
	write(led_fd, end_pattern, 2);
	sleep(1);
	close(led_fd);
	close(us_fd);
	return 0;
}
