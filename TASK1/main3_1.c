/**********************************************************************************************
 * main_3.c  - Display pattern in LED 8x8 matrix based on distance from the ultrasonic sensor
 * 
 * COPYRIGHT (c) JANAKARAJAN NATARAJAN
 *
 *********************************************************************************************/
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <poll.h>
#include <inttypes.h>
#include <pthread.h>

pthread_mutex_t lock;
uint64_t led_sleep_time;
int thread_join_flag;

/****************************************************
 * FUNCTION: Read Time Stamp Counter
 *
 * RETURNS: 64-bit time stamp value
 *
 ****************************************************/
uint64_t inline rdtsc(){
	unsigned int low, high;
	asm volatile("rdtsc"
			: "=a" (low), "=d" (high));
	return (uint64_t)high << 32 | low;
}

/*****************************************************************************
 * FUNCTION: Initialize the Galileo GPIO to control LED and ultrasonic sensor
 *
 * RETURNS:  0 Success
 *
 ****************************************************************************/ 
int init_gpio(){
	int fd, fd2, fd3;
	int i;
	char buf[100];
	int gpio[] = {31, 30, 14, 15, 42, 43, 55};

	for(i = 0; i < 7; i++){
		fd = open("/sys/class/gpio/export", O_WRONLY);
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d", gpio[i]);
		write(fd, buf, strlen(buf));

		memset(buf, 0, sizeof(buf));
		sprintf(buf, "/sys/class/gpio/gpio%d/direction", gpio[i]);
		fd2 = open(buf, O_WRONLY);
		if(gpio[i] == 15){
			write(fd, "in", 2);
		}
		else{
			write(fd2, "out", 3);
		}

		memset(buf, 0, sizeof(buf));
		sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio[i]);
		fd3 = open(buf, O_WRONLY);
		write(fd3, "0", 1);

		close(fd);
		close(fd2);
		close(fd3);
	}
	return 0;
}

/*********************************************************************
 * FUNCTION: Close the opened GPIO
 *
 * RETURNS: 0 Success
 *
 ********************************************************************/
int close_gpio(){
	int fd;
	int i;
	char buf[10];
	int gpio[] = {31, 30, 14, 15, 42, 43, 55};
	
	fd = open("/sys/class/gpio/unexport", O_WRONLY);
	for(i = 0; i < 7; i++){
		memset(buf, 0, sizeof(buf));
		sprintf(buf, "%d", gpio[i]);
		write(fd, buf, strlen(buf));
	}
	close(fd); 
	return 0;
}

/********************************************************************
 * FUNCTION: Send spi config messages
 *
 * PARAM: fd  - file descriptor
 *        buf - buffer containing data to be sent
 *
 *******************************************************************/
int send_spi_message(int fd, char *buf){
	int status;
	struct spi_ioc_transfer tr;

	memset((void*)&tr, 0, sizeof(struct spi_ioc_transfer));
	tr.tx_buf = (unsigned long)buf;
	tr.len = 2;
	tr.bits_per_word = 8;
	tr.speed_hz = 10000000;
	tr.delay_usecs = 1;

	status = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	if(status < 0){
		printf("\nERROR in sending SPI message %d", errno);
	}
	return status;
}

/******************************************************************
 * FUNCTION: Send the pattern to be displayed to the LED
 *
 * PARAM: fd  - file descriptor
 *        buf - buffer containing data
 *
 ****************************************************************/	
int send_spi_message1(int fd, char *buf){
	int status, d, i;
	char t_buf[2];
	struct spi_ioc_transfer tr;

	memset((void*)&tr, 0, sizeof(struct spi_ioc_transfer));
	for(i = 0; i < 8; i++){
		memset(t_buf, 0, sizeof(t_buf));
		d = i;
		t_buf[0] = buf[i + d];
		d++;
		t_buf[1] = buf[i + d];
		tr.tx_buf = (unsigned long)t_buf;
		tr.len = 2;
		tr.bits_per_word = 8;
		tr.speed_hz = 10000000;
		tr.delay_usecs = 1;
		status = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
		if(status < 0){
			printf("\nERROR in sending SPI message %d", errno);
		}
	}
	return status;
}

/*****************************************************************
 * FUNCTION: Initialize LED. Set the control registers
 *
 ****************************************************************/
int init_led(){
	int fd, status;
	char buf[2];	

	fd = open("/dev/spidev1.0", O_RDWR);
	if(fd < 0){
		printf("\nCould not open file");
		return 1;
	}

	memset(buf, 0, sizeof(buf));
	buf[0] = 0x0C;
	buf[1] = 0x01;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSUCCESS IN SHUTDOWN");
	}

	memset(buf, 0, sizeof(buf));
	buf[0] = 0x0F;
	buf[1] = 0x00;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSUCCESS IN SHUTDOWN");
	}
	
	memset(buf, 0, sizeof(buf));
	buf[0] = 0x0a;
	buf[1] = 0x0f;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSUCCESS IN INTENSITY");
	}

	memset(buf, 0, sizeof(buf));
	buf[0] = 0x0b;
	buf[1] = 0x07;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSUCCESS IN SCAN");
	}

	memset(buf, 0, sizeof(buf));
	buf[0] = 0x09;
	buf[1] = 0x00;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSETTING DISPLAY TEST");
	}

	memset(buf, 0, sizeof(buf));
	buf[0] = 0x0C;
	buf[0] = 0x01;
	status = send_spi_message(fd, buf);
	if(status > 0){
		//printf("\nSETTING ROW");
	}
	close(fd);
	return 0;
}

/**************************************************************
 * FUNCTION: Thread function to measure the ultrasonic value
 *
 * NOTES: Writes a high trigger to sensor for 10ms
 *        Reads the rising edge and falling edge
 *	  Calculates pulse width based on time difference
 *	  Sets the time stamp value to global variable
 *
 ************************************************************/
void* us_meas(){
	int fd, us_fd, value_fd, status, i = 0, miss = 0;
	char rising[10];
	char falling[10];
	char d_rbuf[64];
	uint64_t time1, time2, time_diff, mhz = 400000000;
	struct pollfd poll_us_sensor;

	sprintf(rising, "rising");
	sprintf(falling, "falling");
	fd = open("/sys/class/gpio/gpio14/value", O_WRONLY);
	if(fd < 0){
		printf("\nCould not open Trigger pin");
	}

	us_fd = open("/sys/class/gpio/gpio15/edge", O_WRONLY);
	if(us_fd < 0){
		printf("\nCould not open Echo pin");
	}

	value_fd = open("/sys/class/gpio/gpio15/value", O_RDONLY);
	if(value_fd < 0){
		printf("\nCould not open Echo value");
	}
	
	while(1){
		if(thread_join_flag == 1){
			break;
		}
		memset((void*)&poll_us_sensor, 0, sizeof(struct pollfd));
		poll_us_sensor.fd = value_fd;
		poll_us_sensor.events = POLLPRI;
		poll_us_sensor.revents = 0;
		write(us_fd, rising, strlen(rising));
		write(fd, "1", 1);
		usleep(10);
		write(fd, "0", 1);
		status = poll(&poll_us_sensor, 1, 1000);
		if(status > 0){
			if(poll_us_sensor.revents & POLLPRI){
				read(value_fd, d_rbuf, sizeof(d_rbuf));
				time1 = rdtsc();						/* Read timestamp for rising edge */
				write(us_fd, falling, strlen(falling));
			}
			status = poll(&poll_us_sensor, 1, 1000);
			if(status > 0){
				if(poll_us_sensor.revents & POLLPRI){
					read(value_fd, d_rbuf, sizeof(d_rbuf));
					time2 = rdtsc();					/* Read timestamp for falling edge */
				}
			}
			else{
				miss = 1;
			}
		}
		if(miss != 1){
			pthread_mutex_lock(&lock);
			led_sleep_time = time2 - time1;
			pthread_mutex_unlock(&lock);
		}
		miss = 0;
	}
	close(value_fd);
	close(us_fd);
	close(fd);
	pthread_exit(0);
}

/************************************************************************
 * FUNCTION: Thread function to run the LED pattern
 *
 * NOTES: Reads the timestamp value from global variable
 *	  Sets the pattern based on distance calculated and sleeps
 *
 ***********************************************************************/
void* run_led(void *ptr){
	char zbuf1[] = {
		0x01, 0x08, 0x02, 0x90, 0x03, 0xF0, 0x04, 0x10, 				/* Patterns to be displayed */
		0x05, 0x10, 0x06, 0x37, 0x07, 0xDF, 0x08, 0x98
	};
	char zbuf2[] = {
		0x01, 0x20, 0x02, 0x10, 0x03, 0x70, 0x04, 0xD0, 
		0x05, 0x10, 0x06, 0x97,0x07, 0xFF, 0x08, 0x18
	};
	char zbuf3[] = {
		0x01, 0x98, 0x02, 0xDF, 0x03, 0x37, 0x04, 0x10,
		0x05, 0x10, 0x06, 0xF0, 0x07, 0x90, 0x08, 0x08
	};
	char zbuf4[] = {
		0x01, 0x18, 0x02, 0xFF, 0x03, 0x97, 0x04, 0x10,
		0x05, 0xD0, 0x06, 0x70, 0x07, 0x10, 0x08, 0x20
	};
	long double local_new_sleep = 0;
	long double local_sleep = 0;
	uint64_t temp_sleep;
	int fd, i, status;

	init_led();
	fd = open("/dev/spidev1.0", O_RDWR);
	if(fd < 0){
		printf("\nCould not open file");
	}

	while(1){
		if(thread_join_flag == 1){
			break;
		}
		pthread_mutex_lock(&lock);
		temp_sleep = led_sleep_time;								/* Get global timestamp value */
		pthread_mutex_unlock(&lock);
		local_sleep = (long double)temp_sleep/(long double)400000000;				/* Calculate distance from timestamp value */
		local_sleep = (340 * local_sleep)/2;
		local_sleep = local_sleep * 100;

		if(local_sleep < 10.0){
			status = send_spi_message1(fd, zbuf1);
			if(status < 0){
				printf("\nSEND ERROR");
			}
			usleep(temp_sleep);
			status = send_spi_message1(fd, zbuf2);
			if(status < 0){
				printf("\nERROR 2");
			}
			usleep(temp_sleep);
		}
		else{
			status = send_spi_message1(fd, zbuf3);
			if(status < 0){
				printf("\nSEND ERROR");
			}
			usleep(temp_sleep);
			status = send_spi_message1(fd, zbuf4);
			if(status < 0){
				printf("\nERROR 2");
			}
			usleep(temp_sleep);
		}
	}
	close(fd);
	pthread_exit(0);
}

/****************************************************************
 * FUNCTION: Main thread
 *
 * NOTES: Spawns thread for measuring ultrasonic value
 *	  Spawns thread for displaying pattern in LED matrix
 *	  Sleeps for user specified time
 *	  Sets global join variable
 * 	  Waits for threads to join
 *
 ****************************************************************/
int main(int argc, char **argv){
	int fd, i;
	int status;
	int time_to_execute;
	pthread_t thread_led, thread_us;

	thread_join_flag = 0;
	printf("ENTER NUMBER OF SECONDS TO EXECUTE: ");
	scanf("%d", &time_to_execute);
	
	init_gpio();
	pthread_mutex_init(&lock, NULL);
	status = pthread_create(&thread_led, NULL, &run_led, NULL);
	status = pthread_create(&thread_us, NULL, &us_meas, NULL);

	sleep(time_to_execute);
	thread_join_flag = 1;
	pthread_join(thread_led, NULL);
	pthread_join(thread_us, NULL);
	pthread_mutex_destroy(&lock);
	close_gpio();
	return 0;
}
